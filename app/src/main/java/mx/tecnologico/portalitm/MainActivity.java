package mx.tecnologico.portalitm;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        checkFirstRun();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.botones, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.logout) {
            logOut();
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.encuestas) {
            String url = "http://portal.itmerida.mx:7778/portal/page/portal/port_alum/enc_auserv";
            Intent mainIntent = new Intent().setClass(this, Portal.class);
            mainIntent.putExtra("url", url);
            startActivity(mainIntent);
        }
        if (id == R.id.horarios) {
            String url = "http://portal.itmerida.mx:7778/portal/page/portal/port_alum/horarios/";
            Intent mainIntent = new Intent().setClass(this, Portal.class);
            mainIntent.putExtra("url", url);
            startActivity(mainIntent);
        } else if (id == R.id.aiura) {
            showFaq();
        } else if (id == R.id.about) {
            Toast.makeText(this, getString(R.string.texto_about), Toast.LENGTH_SHORT).show();
        } else if (id == R.id.logoutM) {
            logOut();
        } else if (id == R.id.donar){
            Toast.makeText(MainActivity.this, "¡Gracias por ayudarme!", Toast.LENGTH_SHORT).show();
            String url = "https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=X4JWB77LVGASC";
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(browserIntent);
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void logOut() {
        Toast.makeText(this, "Cerrando sesión", Toast.LENGTH_SHORT).show();
        String url ="http://portal.itmerida.mx:7778/portal/pls/portal/PORTAL.wwsec_app_priv.logout?p_return_url=http%3A%2F%2Fportal.itmerida.mx%3A7778%2Fportal%2Fpage%2Fportal%2Fport_alum";
        Intent mainIntent = new Intent().setClass(this, Portal.class);
        mainIntent.putExtra("url", url);
        startActivity(mainIntent);
    }

    public void calif(View view) {
        String url = "http://portal.itmerida.mx:7778/portal/page/portal/port_alum/calif_alum/";
        Intent mainIntent = new Intent().setClass(this, Portal.class);
        mainIntent.putExtra("url", url);
        startActivity(mainIntent);
    }

    public void seguimiento(View view) {
        String url = "http://portal.itmerida.mx:7778/portal/page/portal/port_alum/seg_esc/";
        Intent mainIntent = new Intent().setClass(this, Portal.class);
        mainIntent.putExtra("url", url);
        startActivity(mainIntent);
    }

    public void carga(View view) {
        String url = "http://portal.itmerida.mx:7778/portal/page/portal/port_alum/carga_acad_alum/";
        Intent mainIntent = new Intent().setClass(this, Portal.class);
        mainIntent.putExtra("url", url);
        startActivity(mainIntent);
    }

    public void docente(View view) {
        String url = "http://portal.itmerida.mx:7778/portal/page/portal/port_alum/eva_doc/";
        Intent mainIntent = new Intent().setClass(this, Portal.class);
        mainIntent.putExtra("url", url);
        startActivity(mainIntent);
    }

    public void showFaq() {
        String a = getString(R.string.texto_faq);
        new AlertDialog.Builder(this).setTitle("¡Hola!").setMessage(a).setNeutralButton("Está bien", null).show();
    }

    public void checkFirstRun() {
        boolean isFirstRun = getSharedPreferences("PREFERENCE", MODE_PRIVATE).getBoolean("isFirstRun", true);
        if (isFirstRun) {
            showFaq();
            getSharedPreferences("PREFERENCE", MODE_PRIVATE)
                    .edit()
                    .putBoolean("isFirstRun", false)
                    .apply();
        }
    }
}
