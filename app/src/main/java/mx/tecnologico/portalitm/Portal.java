package mx.tecnologico.portalitm;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.CookieManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

public class Portal extends AppCompatActivity {

    String logout = "http://portal.itmerida.mx:7777/pls/orasso/orasso.wwsso_app_admin.ls_logout?p_done_url=http://portal.itmerida.mx:7778/portal/page/portal/port_alum";
    WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_portal);

        String url = getIntent().getExtras().getString("url");
        webView = (WebView) findViewById(R.id.webView);

        webView.setWebViewClient(new WebViewClient() {
            public void onPageFinished(WebView webView, String url) {
                if (url.equals(logout)) {
                    finish();
                }
            }
        });
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDisplayZoomControls(false);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.scrollTo(350,370);
        webView.setInitialScale(240);
        webView.loadUrl(url);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.boton_portal, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.recargar) {
            webView.reload();
            Toast.makeText(Portal.this, "Recargando", Toast.LENGTH_SHORT).show();
        } else if (id == R.id.copiar) {
            String Z = webView.getUrl();
            android.content.ClipboardManager clipboard = (android.content.ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
            android.content.ClipData clip = android.content.ClipData.newPlainText(":D", Z);
            clipboard.setPrimaryClip(clip);
            Toast.makeText(Portal.this, "URL copiado", Toast.LENGTH_SHORT).show();
        } else if (id == R.id.wipe) {
            webView.clearCache(true);
            webView.reload();
            CookieManager cookieManager = CookieManager.getInstance();
            cookieManager.removeAllCookie();
            Toast.makeText(Portal.this, "Vaciando caché ♥", Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }

}



